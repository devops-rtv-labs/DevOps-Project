#!/usr/bin/env sh

# Validate if those tools are available on the system

failed () { echo "Failed:" >&2 "$@" && exit 1; }    

validate_function () {
    GREEN='\033[1;32m'
    RED='\033[1;31m'
    NC='\033[0m'

    echo -n "Running $1 "
    ERROR=$( $1 2>&1 )
    [ $? -eq 0 ] && echo -e "[ ${GREEN}DONE${NC} ]" ||  (echo -e "[ ${RED}FAILED${NC} ]" && failed "Unexpected error occurred: ${ERROR}, please install the tool to start contributing. Follow this link: ${2}")

    # print version otherwise
    [ $? -eq 0 ] && $1
}

validate_function "git --version" "https://git-scm.com/book/en/v2/Getting-Started-Installing-Git"
validate_function "go version" "https://golang.org/doc/install"
validate_function "docker --version" "https://docs.docker.com/get-docker/"
validate_function "docker-compose --version" "https://docs.docker.com/compose/install/"
validate_function "kubectl version --client" "https://kubernetes.io/docs/tasks/tools/install-kubectl/"
validate_function "helm version --client" "https://helm.sh/docs/intro/install/"
validate_function "kind version" "https://kind.sigs.k8s.io/docs/user/quick-start/" 
validate_function "terraform version" "https://learn.hashicorp.com/tutorials/terraform/install-cli"
validate_function "node -v" "https://nodejs.org/en/download/"
