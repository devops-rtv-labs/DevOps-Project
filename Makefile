# Validate
validate:
	@echo -e "\033[0;32m[*] Validating...\033[0m"
	@./hack/validate-tools.sh
	@echo -e "\033[0;32m[*] Validated successfully!\033[0m"

# Init
init:
	@echo -e "\033[0;32m[*] Initializing Development Environment...\033[0m"
	@cd config/iac && terraform init
	@echo -e "\033[0;32m[*] Initialized Development Environment successfully!\033[0m"
	@echo -e "\033[0;32m[*] Apply Development Environment...\033[0m"
	@cd config/iac && terraform apply -auto-approve
	@echo -e "\033[0;32m[*] Applied Development Environment successfully!\033[0m"
	@make argocd
	@make rabbitmq

# Development
api-server:
	@echo -e "\033[0;32m[*] Building API server...\033[0m"
	@mkdir -p bin
	@cd apps && go build -o ../bin/api-server ./cmd/api-server/main.go
	@echo -e "\033[0;32m[*] API server built successfully!\033[0m"
	@./bin/api-server

recommender:
	@echo -e "\033[0;32m[*] Building recommender...\033[0m"
	@mkdir -p bin
	@cd apps && go build -o ../bin/recommender ./cmd/recommender/main.go
	@echo -e "\033[0;32m[*] Recommender built successfully!\033[0m"
	@./bin/recommender

swagger:
	@echo -e "\033[0;32m[*] Running swagger...\033[0m"
	@mkdir -p bin
	@go install github.com/swaggo/swag/cmd/swag@latest
	@cd apps && swag init --parseDependency -d internal/handlers -g echo-handler.go -o internal/core/api-docs
	@cd apps && go build -o ../bin/swagger ./cmd/swagger/main.go
	@echo -e "\033[0;32m[*] Swagger built successfully!\033[0m"
	@echo -e "\033[0;32m[*] Run ./bin/swagger to start the server.\033[0m"
	@./bin/swagger 

web-app:
	@echo -e "\033[0;32m[*] Running web app...\033[0m"
	@cd apps/web && npm run dev

storybook:
	@echo -e "\033[0;32m[*] Running storybook...\033[0m"
	@cd apps/web && npm run storybook

# Environments
test-env:
	@echo -e "\033[0;32m[*] Building test environment...\033[0m"
	@cd tmp && docker-compose up -d
	@echo -e "\033[0;32m[*] Built test environment successfully!\033[0m"

# Tests

test:
	@echo -e "\033[0;32m[*] Running unit tests...\033[0m"
	@mkdir -p dist
	@cd apps && go test -v ./...
	@cd apps && go test -cover ./... -coverprofile=../dist/coverage.out -json > ../dist/test-report.json
	@echo -e "\033[0;32m[*] Unit tests ran successfully!\033[0m"
	@echo -e "\033[0;32m[*] Generating Coverage Report...\033[0m"
	@cd apps && go tool cover -html=../dist/coverage.out -o ../dist/coverage.html
	@echo -e "\033[0;32m[*] Coverage Report generated successfully!\033[0m"


# Deployment
apply:
	@echo -e "\033[0;32m[*] Applying changes...\033[0m"
	@cd config/k8s && kubectl apply -k .
	@echo -e "\033[0;32m[*] Applied changes successfully!\033[0m"


# RabbitMQ
rabbitmq: # Install RabbitMQ on the cluster.
	@echo -e "\033[0;33mInstalling RabbitMQ...\033[0m"
	@kubectl apply -f https://github.com/rabbitmq/cluster-operator/releases/latest/download/cluster-operator.yml

# ArgoCD
argocd:
	@kubectl apply -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
	@kubectl patch svc argocd-server -p '{"spec": {"type": "NodePort", "ports": [{"port": 80, "targetPort": 8080, "nodePort": 31003}]}}'
	@echo -e "\033[0;32m[*] ArgoCD installed successfully! Access it at http://localhost:9001\033[0m"

# Build

build-api-server:
	@echo -e "\033[0;32m[*] Building API server...\033[0m"
	@mkdir -p bin
	@cd apps && go build -o ../bin/api-server ./cmd/api-server/main.go
	@echo -e "\033[0;32m[*] API server built successfully!\033[0m"

build-recommender:
	@echo -e "\033[0;32m[*] Building recommender...\033[0m"
	@mkdir -p bin
	@cd apps && go build -o ../bin/recommender ./cmd/recommender/main.go
	@echo -e "\033[0;32m[*] Recommender built successfully!\033[0m"

build-swagger:
	@echo -e "\033[0;32m[*] Running swagger...\033[0m"
	@mkdir -p bin
	@go install github.com/swaggo/swag/cmd/swag@latest
	@cd apps && swag init --parseDependency -d internal/handlers -g echo-handler.go -o internal/core/api-docs
	@cd apps && go build -o ../bin/swagger ./cmd/swagger/main.go
	@echo -e "\033[0;32m[*] Swagger built successfully!\033[0m"
	@echo -e "\033[0;32m[*] Run ./bin/swagger to start the server.\033[0m"

build-web-app:
	@echo -e "\033[0;32m[*] Building web app...\033[0m"
	@mkdir -p dist
	@cd apps/web && npm run build
	@echo -e "\033[0;32m[*] Web app built successfully!\033[0m"

build-storybook:
	@echo -e "\033[0;32m[*] Building storybook...\033[0m"
	@mkdir -p dist
	@cd apps/web && npm run build-storybook -- -o ../../dist/storybook
	@echo -e "\033[0;32m[*] Storybook built successfully!\033[0m"

build: build-api-server build-recommender build-swagger build-web-app build-storybook

# Load Testing
test-load:
	@mkdir -p dist/k6
	@k6 run -e BASE_URL=http://localhost:8082 --out json=dist/k6/load.json ./hack/load/load-test.js

test-stress:
	@mkdir -p out
	@k6 run -e BASE_URL=http://localhost:31000/api --out json=dist/k6/stress.json ./hack/load/stress-test.js

test-spike:
	@mkdir -p out
	@k6 run -e BASE_URL=http://localhost:31000/api --out json=dist/k6/spike.json ./hack/load/spike-test.js




# Clean

clean:
	@echo -e "\033[0;32m[*] Cleaning up...\033[0m"
	@rm -rf bin
	@rm -rf dist
	@rm -f config/iac/terraform.tfstate config/iac/terraform.tfstate.backup
	@echo -e "\033[0;32m[*] Cleaned up successfully!\033[0m"


# Phonies
.PHONY: api-server recommender web-app storybook swagger validate init apply test-env unit test
.PHONY: build-api-server build-recommender build-swagger build-web-app build-storybook
.PHONY: clean test-load test-stress test-spike