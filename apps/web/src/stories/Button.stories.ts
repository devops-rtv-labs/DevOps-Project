import type { Meta, StoryObj } from "@storybook/react";
import "../index.css";

import { Button } from "../ui";

const meta = {
  title: "UI/Button",
  component: Button,
  tags: ["autodocs"],
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Full: Story = {
  args: {
    kind: "full",
    children: "Button",
  },
};

export const Outline: Story = {
  args: {
    kind: "outline",
    children: "Button",
  },
};
