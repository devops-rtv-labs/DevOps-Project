import type { Meta, StoryObj } from "@storybook/react";
import "../index.css";

import { Input } from "../ui";

const meta = {
  title: "UI/Input",
  component: Input,
  tags: ["autodocs"],
} satisfies Meta<typeof Input>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Base: Story = {
  args: {
    value: "Hello World",
    placeholder: "Placeholder",
  },
};
