import type { Meta, StoryObj } from "@storybook/react";
import "../index.css";

import { Card } from "../ui";

const meta = {
  title: "UI/Card",
  component: Card,
  tags: ["autodocs"],
} satisfies Meta<typeof Card>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    children: "Hello World, I am a card",
  },
};
