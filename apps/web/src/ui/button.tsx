import { VariantProps, cva } from "class-variance-authority";


const button = cva(
  "px-4 py-2 rounded-md font-bold focus:ring-4 focus:ring-indigo-400 focus:ring-opacity-50 duration-300",
  {
    variants: {
      kind: {
        full: "bg-indigo-500 text-white ",
        outline: "bg-transparent border-2 border-indigo-500 text-indigo-500",
      },
    },
    defaultVariants: {
      kind: "full",
    },
  }
);


export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement>,
    VariantProps<typeof button> {
  kind?: "full" | "outline";
    }

export const Button: React.FC<ButtonProps> = ({
  className,
  kind,
  ...props
}) => <button className={button({ className, kind })} {...props} />;
