import { VariantProps, cva } from "class-variance-authority";

const card = cva("p-10 bg-white shadow-md rounded-lg");

export interface CardProps
  extends React.HTMLAttributes<HTMLDivElement>,
    VariantProps<typeof card> {}

export const Card: React.FC<CardProps> = ({ className, ...props }) => (
  <div className={card({ className })} {...props} />
);
