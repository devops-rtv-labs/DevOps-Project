import { VariantProps, cva } from "class-variance-authority";

const input = cva(
  "border rounded-md focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:border-transparent px-4 py-2 w-full duration-300"
);

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement>,
    VariantProps<typeof input> {}

export const Input: React.FC<InputProps> = ({ className, ...props }) => (
  <input className={input({ className })} {...props} />
);
