import { useEffect } from "react";

export interface Place {
  name: string;
}

export interface Recommendation {
  places: Place[];
}

export function useRecommendation(cb: (data: Recommendation) => void) {
  useEffect(() => {
    const socket = new WebSocket("ws://" + window.location.host + "/api/ws");
    socket.onmessage = (event) => {
      const data = JSON.parse(event.data);
      console.log(data);
      cb(data);
    };

    return () => {
      socket.close();
    };
  }, [cb]);
}
