import axios from "axios";
import { useCallback } from "react";
import toast from "react-hot-toast";
import { Place, useRecommendation } from "./use-socket";
import { Button, Card, Input } from "./ui";

function App() {
  useRecommendation((d) => {
    if (d.places && d.places.length !== 0) {
      const recommendation = d.places.reduce((acc: string, place: Place) => {
        return acc + place.name + ", ";
      }, "");

      toast.success("You should go to " + recommendation);
    }
  });

  const submit = useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);

    const feeling = (data.get("feeling") || "").toString();

    toast.promise(axios.post("/api/", { feeling }), {
      loading: "Sending your feeling...",
      success: "Your feeling was sent!",
      error: "Something went wrong!",
    });

    e.currentTarget.reset();
  }, []);

  return (
    <>
      <div className="h-screen w-screen bg-gray-50 grid place-content-center">
        <Card>
          <form onSubmit={submit}>
            <h1 className="font-black uppercase text-2xl text-indigo-700">
              Recommend me for Places to go
            </h1>

            <div className="py-5">
              <label>
                <h3 className="italic font-light text-sm pb-4">
                  Write some stuff you want to feel
                </h3>

                <Input type="text" name="feeling" />
              </label>
            </div>
            <div className="flex justify-end">
              <Button>Send</Button>
            </div>
          </form>
        </Card>
      </div>
    </>
  );
}

export default App;