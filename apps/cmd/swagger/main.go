// Swagger HTTP Server
// This is a simple HTTP server that serves the Swagger API documentation
package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"go.uber.org/zap"

	_ "gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/api-docs"
)

// main is the entrypoint of the application
// It creates a logger and an echo instance
// It starts the echo instance to serve the api-docs
func main() {
	// Create logger (zap)
	logger, die := common.NewLogger()
	defer die()

	// Create echo instance
	e := echo.New()

	// CORS
	e.Use(middleware.CORS())

	// Inject Logger into echo
	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogURI:    true,
		LogStatus: true,
		LogValuesFunc: func(c echo.Context, v middleware.RequestLoggerValues) error {
			logger.Info("request",
				zap.String("method", v.Method),
				zap.String("uri", v.URI),
				zap.Int("status", v.Status),
				zap.String("latency", v.Latency.String()),
			)
	
			return nil
		},
	}))

	e.GET("/*", echoSwagger.WrapHandler)

	e.Logger.Fatal(e.Start(":8081"))
}