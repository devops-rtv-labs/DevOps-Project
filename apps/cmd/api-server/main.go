// API Server
// This is the API Server that will handle the HTTP requests from the client
package main

import (
	"github.com/labstack/echo-contrib/jaegertracing"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/agent"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/config"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/handlers"
	"gitlab.com/devops-rtv-labs/DevOps-Project/pkg/repositories"
	"go.uber.org/zap"
)

// main is the entrypoint of the application
// It creates a logger, an echo instance, a rabbitmq repository, an agent service and an agent handler
// It starts the agent handler
func main() {
	// Create logger (zap)
	logger, die := common.NewLogger()
	defer die()

	// Create Config
	cfg := config.NewConfig()
	
	// Create echo instance
	e := echo.New()

	// CORS
	e.Use(middleware.CORS())

	// Inject Logger into echo
	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogURI:    true,
		LogStatus: true,
		LogValuesFunc: func(c echo.Context, v middleware.RequestLoggerValues) error {
			logger.Info("request",
				zap.String("method", v.Method),
				zap.String("uri", v.URI),
				zap.Int("status", v.Status),
				zap.String("latency", v.Latency.String()),
			)
	
			return nil
		},
	}))
	
	// Inject Jaeger Tracer
	c := jaegertracing.New(e, nil)
    defer c.Close()

	// Create RabbitMQ repository
	mq, err := repositories.NewRabbitMQRepository(
		repositories.WithRabbitMQRepositoryConnection(
			cfg.GetRabbitMQ().Host, cfg.GetRabbitMQ().Port, cfg.GetRabbitMQ().Username, cfg.GetRabbitMQ().Password,
		),
	)
	if err != nil {
		logger.Panicf("[!] failed to create rabbitmq repository: %s", err.Error())
	}

	// Create Agent Service
	service, err := agent.NewAgentServiceImpl(
		agent.WithMessageQueueRepository(mq),
	)
	if err != nil {
		logger.Panicf("[!] failed to create agent service: %s", err.Error())
	}

	// Create Agent Handler
	handler, err := handlers.NewAgentEchoHandler(
		handlers.WithCustomEcho(e),
		handlers.WithAgentService(service),
		handlers.WithCustomConfig(cfg),
	)
	if err != nil {
		logger.Panicf("[!] failed to create agent handler: %s", err.Error())
	}
	
	// Start Agent Handler
	handler.Start()
}