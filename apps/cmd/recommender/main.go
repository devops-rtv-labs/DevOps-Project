// Recommender Client
// This is the client that will launch the Recommender Server
// The Recommender Server is responsible for handling the feelings and generating recommendations
package main

import (
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/config"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/recommender"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/handlers"
	"gitlab.com/devops-rtv-labs/DevOps-Project/pkg/repositories"
)

// main is the entrypoint of the application
// It creates a logger, a rabbitmq repository, a recommender service and a recommender handler
// It starts the recommender handler
func main() {
	logger, die := common.NewLogger()
	defer die()

	// Create Config
	cfg := config.NewConfig()

	mq, err := repositories.NewRabbitMQRepository(
		repositories.WithRabbitMQRepositoryConnection(
			cfg.GetRabbitMQ().Host, cfg.GetRabbitMQ().Port, cfg.GetRabbitMQ().Username, cfg.GetRabbitMQ().Password,
		),
	)
	if err != nil {
		logger.Panicf("[!] failed to create rabbitmq repository: %s", err.Error())
	}

	service, err := recommender.NewRecommenderServiceImpl()

	if err != nil {
		logger.Panicf("[!] failed to create recommender service: %s", err.Error())
	}

	handler, err := handlers.NewRecommenderHandler(
		handlers.WithMessageQueueRepository(mq),
		handlers.WithRecommenderService(service),
	)

	if err != nil {
		logger.Panicf("[!] failed to create recommender handler: %s", err.Error())
	}

	handler.Start()
}