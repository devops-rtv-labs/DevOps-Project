// Fake Queue
// This is an implementation of the MessageQueueRepository interface using a fake queue
// It is used for testing purposes only
package repositories

import (
	"time"

	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"go.uber.org/zap"
)

// FakeMessageQueueRepository is a struct that holds the FakeMessageQueue connection and channel
// It is used for testing purposes only
type FakeMessageQueueRepository struct {
	logger *zap.SugaredLogger
}


// NewFakeMessageQueueRepository creates a new FakeMessageQueueRepository
// It accepts a list of FakeMessageQueueConfiguration functions that will configure the repository
// It returns a FakeMessageQueueRepository and an error
func NewFakeMessageQueueRepository() (*FakeMessageQueueRepository, error) {
	r := &FakeMessageQueueRepository{}

	if r.logger == nil {
		r.logger = common.GetLogger()
	}

	r.logger.Infof("[*] Initiated Fake MQ, Note: This is for testing only")

	return r, nil
}

// Publish publishes a message to a queue
// It accepts a queue name and a message
// It returns an error
func (r *FakeMessageQueueRepository) Publish(queue string, message []byte) error {
	r.logger.Infof("[*] publishing message to Fake queue: %s", queue)
	return nil
}

// Subscribe subscribes to a queue
// It accepts a queue name
// It returns a channel of messages and an error
func (r *FakeMessageQueueRepository) Subscribe(queue string) (<-chan []byte, error) {
	r.logger.Infof("[*] subscribing to Fake queue: %s", queue)
	
	messageChannel := make(chan []byte)

	go func() {
		for {
			messageChannel <- []byte("{\"places\":[{\"name\":\"Cafe\"},{\"name\":\"Restaurant\"},{\"name\":\"Bar\"}]}")
			time.Sleep(2 * time.Second)
		}
	}()
	
	return messageChannel, nil
}