package repositories

import (
	"testing"

	"github.com/stretchr/testify/assert"
)


func TestCreateFMQRepository(t *testing.T) {
	assert.NotPanics(t, func () {
		fmq, err := NewFakeMessageQueueRepository()

		assert.Nil(t, err)
		assert.NotNil(t, fmq)
	})
}


func TestFMQRepositoryPublish(t *testing.T) {
	assert.NotPanics(t, func () {
		fmq, err := NewFakeMessageQueueRepository()

		assert.Nil(t, err)
		assert.NotNil(t, fmq)

		err = fmq.Publish("test-publish", []byte("test"))
		assert.Nil(t, err)
	})
}

func TestFMQRepositorySubscribe(t *testing.T) {
	assert.NotPanics(t, func () {
		fmq, err := NewFakeMessageQueueRepository()

		assert.Nil(t, err)
		assert.NotNil(t, fmq)

		channel, err := fmq.Subscribe("test-subscribe")

		assert.Nil(t, err)

		msg := <-channel

		assert.Equal(t, "{\"places\":[{\"name\":\"Cafe\"},{\"name\":\"Restaurant\"},{\"name\":\"Bar\"}]}", string(msg))
	})
}