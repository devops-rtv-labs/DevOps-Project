package repositories

import (
	"testing"

	"github.com/stretchr/testify/assert"
)


func TestCreateRMQRepository(t *testing.T) {
	assert.NotPanics(t, func () {
		rmq, err := NewFakeMessageQueueRepository(
			// WithRabbitMQRepositoryConnection("localhost", "5672", "guest", "guest"),
		)

		assert.Nil(t, err)
		assert.NotNil(t, rmq)
	})
}

func TestCreateFailedRMQRepository(t *testing.T) {
	assert.NotPanics(t, func () {
		assert.Panics(t, func () {
			mq, err := NewRabbitMQRepository()

			assert.Nil(t, mq)
			assert.NotNil(t, err)
		})
	})
}

func TestRMQRepositoryPublish(t *testing.T) {
	assert.NotPanics(t, func () {
		rmq, err := NewFakeMessageQueueRepository(
			// WithRabbitMQRepositoryConnection("localhost", "5672", "guest", "guest"),
		)

		assert.Nil(t, err)
		assert.NotNil(t, rmq)

		err = rmq.Publish("test-publish", []byte("test"))
		assert.Nil(t, err)
	})
}

func TestRMQRepositorySubscribe(t *testing.T) {
	assert.NotPanics(t, func () {
		rmq, err := NewFakeMessageQueueRepository(
			// WithRabbitMQRepositoryConnection("localhost", "5672", "guest", "guest"),
		)

		assert.Nil(t, err)
		assert.NotNil(t, rmq)

		// channel, err := rmq.Subscribe("test-subscribe")

		// assert.Nil(t, err)

		// err = rmq.Publish("test-subscribe", []byte("test"))

		// assert.Nil(t, err)

		// msg := <-channel

		// assert.Equal(t, "test", string(msg))
	})
}