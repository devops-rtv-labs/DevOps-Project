// RabbitMQ Repository
// This is the implementation of the MessageQueueRepository interface using RabbitMQ
package repositories

import (
	"context"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"go.uber.org/zap"
)

// RabbitMQConfiguration is a function that configures a RabbitMQRepository
type RabbitMQConfiguration func(r *RabbitMQRepository) error

// RabbitMQRepositoryConnectionConfig is a struct that holds the RabbitMQ connection configuration
type RabbitMQRepositoryConnectionConfig struct {
	Host     string
	Port     string
	User     string
	Password string
}

// RabbitMQRepository is a struct that holds the RabbitMQ connection and channel
type RabbitMQRepository struct {
	*RabbitMQRepositoryConnectionConfig
	*amqp.Channel
	logger *zap.SugaredLogger
}

// NewRabbitMQRepository creates a new RabbitMQRepository
// It accepts a list of RabbitMQConfiguration functions that will configure the repository
// It returns a RabbitMQRepository and an error
func NewRabbitMQRepository(cfgs ...RabbitMQConfiguration) (*RabbitMQRepository, error) {
	r := &RabbitMQRepository{}
	for _, cfg := range cfgs {
		err := cfg(r)
		if err != nil {
			return nil, err
		}
	}

	if r.logger == nil {
		r.logger = common.GetLogger()
	}

	amqpURI := r.getAmqpURI()

	conn, err := amqp.Dial(amqpURI)
	if err != nil {
		r.logger.Panicf("[!] Failed to connect to RabbitMQ: %s", err)
	} else {
		r.logger.Infof("[*] Connected to RabbitMQ")
	}

	ch, err := conn.Channel()
	if err != nil {
		r.logger.Panicf("[!] Failed to open a channel: %s", err)
	} else {
		r.logger.Infof("[*] Opened a channel")
	}
	r.Channel = ch
	return r, nil
}

// getAmqpURI returns the AMQP URI
func (r *RabbitMQRepository) getAmqpURI() string {
	return "amqp://" + r.User + ":" + r.Password + "@" + r.Host + ":" + r.Port + "/"
}

// Publish publishes a message to a queue
// It accepts a queue name and a message as a byte array
// It returns an error
func (r *RabbitMQRepository) Publish(queue string, message []byte) error {
	r.logger.Infof("[*] publishing message to queue: %s", queue)
	ctx := context.Background()
	_, err := r.Channel.QueueDeclare(
		queue,
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return err
	}

	err = r.Channel.PublishWithContext(
		ctx,
		"",
		queue,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        message,
		},
		
	)

	if err != nil {
		return err
	}

	return nil
}

// Subscribe subscribes to a queue
// It accepts a queue name
// It returns a channel of byte arrays and an error
func (r *RabbitMQRepository) Subscribe(queue string) (<-chan []byte, error) {
	r.logger.Infof("[*] subscribing to queue: %s", queue)
	
	_, err := r.Channel.QueueDeclare(
		queue,
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return nil, err
	}

	msgs, err := r.Channel.Consume(
		queue,
		"",
		true,
		false,
		false,
		false,

		nil,
	)

	if err != nil {
		return nil, err
	}

	messageChannel := make(chan []byte)

	go func() {
		for d := range msgs {
			messageChannel <- d.Body
		}
	}()

	return messageChannel, nil
}

// WithRabbitMQRepositoryLogger sets configuration for RabbitMQRepository
func WithRabbitMQRepositoryConnection(
	host string,
	port string,
	user string,
	password string,
) RabbitMQConfiguration {
	return func(r *RabbitMQRepository) error {
		if r.RabbitMQRepositoryConnectionConfig == nil {
			r.RabbitMQRepositoryConnectionConfig = &RabbitMQRepositoryConnectionConfig{}
		}

		r.RabbitMQRepositoryConnectionConfig.Host = host
		r.RabbitMQRepositoryConnectionConfig.Port = port
		r.RabbitMQRepositoryConnectionConfig.User = user
		r.RabbitMQRepositoryConnectionConfig.Password = password
		return nil
	}
}