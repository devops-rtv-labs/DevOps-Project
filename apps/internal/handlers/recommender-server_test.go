package handlers

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/recommender"
	"gitlab.com/devops-rtv-labs/DevOps-Project/pkg/repositories"
)

func TestCreateRecommenderServer(t *testing.T) {
	assert.NotPanics(t, func () {
		server, err := NewRecommenderHandler()
		assert.Nil(t, err)
		assert.NotNil(t, server)
	})
}


func TestFailedRecommenderServer(t *testing.T) {
	assert.NotPanics(t, func () {
		server, err := NewRecommenderHandler(
			func (a *RecommenderHandler) error {
				return errors.New("error")
			},
		)
		assert.NotNil(t, err)
		assert.Nil(t, server)
	})
}

func TestStart(t *testing.T) {

	mq, err := repositories.NewFakeMessageQueueRepository()
	assert.Nil(t, err)

	service, err := recommender.NewRecommenderServiceImpl()
	assert.Nil(t, err)
	
	server, err := NewRecommenderHandler(
		WithMessageQueueRepository(mq),
		WithRecommenderService(service),
	)
	assert.Nil(t, err)
	assert.NotNil(t, server)
}