// Agent Echo Handler
// This is the AgentHandler implementation using Echo
package handlers

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/config"
	"go.uber.org/zap"
)

var (
	// upgrader is used to upgrade the HTTP connection to a WebSocket connection
	upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
)

// AgentEchoHandler is a struct that implements the AgentHandler interface using Echo
type AgentEchoHandler struct {
	echo *echo.Echo
	service core.AgentService
	logger *zap.SugaredLogger
	cfg *config.Config
}

// AgentConfiguration is a function that configures a AgentEchoHandler
type AgentConfiguration func (a *AgentEchoHandler) error

// NewAgentEchoHandler creates a new AgentEchoHandler
// @title Recommender API Server
// @version 1.0
// @description This is a server for the Recommender API.

// @host localhost:8080
// @BasePath /
func NewAgentEchoHandler(cfgs ...AgentConfiguration) (*AgentEchoHandler, error) {
	h := &AgentEchoHandler{}

	for _, cfg := range cfgs {
		err := cfg(h)
		if err != nil {
			return nil, err
		}
	}

	if h.echo == nil {
		h.echo = echo.New()
		h.echo.Use(middleware.Recover())
	}

	if h.cfg == nil {
		h.cfg = config.NewConfig()
	}

	if h.logger == nil {
		h.logger = common.GetLogger()
	}

	return h, nil
}

// SubmitWantedFeelingT Submit wanted feeling
//
//  @Summary      Submit wanted feeling
//  @Description  it will submit wanted feeling from the user in order to get recommendations
//  @Tags         feeling
//  @Accept       json
//	@Param		  feeling	body	core.Feeling	true	"submit wanted feeling"
//  @Success      200
//  @Router       / [post]
func (h *AgentEchoHandler) SubmitWantedFeeling(c echo.Context) error {
		h.logger.Infof("[*] submitting wanted feeling")
		feeling := core.Feeling{}
		err := c.Bind(&feeling)
		if err != nil || (feeling != core.Feeling{} && feeling.Feeling == "") {
			h.logger.Infof("[!] bad request %s", err.Error())
			return c.String(http.StatusBadRequest, "bad request")
		}
		err = h.service.SubmitWantedFeeling(feeling)
		if err != nil {
			h.logger.Infof("[!] internal server error %s", err.Error())
			return c.String(http.StatusInternalServerError, "internal server error")
		}

		return c.String(200, "Hello, World!")
}

// StreamRecommendations Stream recommendations
//
//  @Summary      Stream recommendations
//  @Description  it will stream recommendations to the user
//  @Tags         recommendation
//  @Success      200 {object}  core.Recommendation
//  @Router       /ws [get]
func (h *AgentEchoHandler) StreamRecommendations(c echo.Context) error {
		h.logger.Infof("[*] streaming recommendations")
		ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
		if err != nil {
			h.logger.Infof("[!] internal server error %s", err.Error())
			return c.String(http.StatusInternalServerError, "internal server error")
		}
		defer ws.Close()
		
		recommender, err := h.service.StreamRecommendations()
		
		if err != nil {
			h.logger.Infof("[!] internal server error %s", err.Error())
			return c.String(http.StatusInternalServerError, "internal server error")
		}
		
		for {
			recommendation := <-recommender
			err := ws.WriteJSON(recommendation)
			if err != nil {
				h.logger.Infof("[!] internal server error %s", err.Error())
				return c.String(http.StatusInternalServerError, "internal server error")
			}
		}
}

// Start starts the AgentEchoHandler
// It starts the Echo server
// It returns an error
func (h *AgentEchoHandler) Start() error {
	h.logger.Infof("[*] agent handler started")

	h.echo.POST("/", h.SubmitWantedFeeling)
	h.echo.GET("/ws", h.StreamRecommendations)
	h.echo.Logger.Fatal(h.echo.Start(
		fmt.Sprintf(":%s", h.cfg.GetApiServerPort()),
	))
	return nil
}

// WithCustomEcho configures the AgentEchoHandler to use a custom Echo instance
func WithCustomEcho(echo *echo.Echo) AgentConfiguration {
	return func(a *AgentEchoHandler) error {
		a.echo = echo
		return nil
	}
}

// WithAgentService configures the AgentEchoHandler to use an AgentService
func WithAgentService(service core.AgentService) AgentConfiguration {
	return func(a *AgentEchoHandler) error {
		a.service = service
		return nil
	}
}

// WithCustomConfig configures the AgentEchoHandler to use a custom config
func WithCustomConfig(cfg *config.Config) AgentConfiguration {
	return func(a *AgentEchoHandler) error {
		a.cfg = cfg
		return nil
	}
}