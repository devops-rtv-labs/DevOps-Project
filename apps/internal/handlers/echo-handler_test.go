package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/agent"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/config"
	"gitlab.com/devops-rtv-labs/DevOps-Project/pkg/repositories"
)

func TestCreateEchoAgentHandler(t *testing.T) {
	assert.NotPanics(t, func() {
		agent, err := NewAgentEchoHandler()
		assert.Nil(t, err)
		assert.NotNil(t, agent)
	})
}

func TestSubmitWantedFeeling(t *testing.T) {

	mq, err := repositories.NewFakeMessageQueueRepository()
	assert.Nil(t, err)

	service, err := agent.NewAgentServiceImpl(
		agent.WithMessageQueueRepository(mq),
	)
	assert.Nil(t, err)

	agent, err := NewAgentEchoHandler(
		WithAgentService(service),
	)
	assert.Nil(t, err)
	assert.NotNil(t, agent)

	req := httptest.NewRequest(http.MethodPost, "/", nil)
	rec := httptest.NewRecorder()
	ctx := agent.echo.NewContext(req, rec)

	err = agent.SubmitWantedFeeling(ctx)
	assert.Nil(t, err)
}

func TestStreamRecommendations(t *testing.T) {
	mq, err := repositories.NewFakeMessageQueueRepository()
	assert.Nil(t, err)

	service, err := agent.NewAgentServiceImpl(
		agent.WithMessageQueueRepository(mq),
	)
	assert.Nil(t, err)

	agent, err := NewAgentEchoHandler(
		WithAgentService(service),
	)
	assert.Nil(t, err)
	assert.NotNil(t, agent)

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	ctx := agent.echo.NewContext(req, rec)

	err = agent.StreamRecommendations(ctx)
	assert.Nil(t, err)
}

func TestStartAgent(t *testing.T) {
	mq, err := repositories.NewFakeMessageQueueRepository()
	assert.Nil(t, err)

	service, err := agent.NewAgentServiceImpl(
		agent.WithMessageQueueRepository(mq),
	)
	assert.Nil(t, err)

	agent, err := NewAgentEchoHandler(
		WithAgentService(service),
	)
	assert.Nil(t, err)
	assert.NotNil(t, agent)

	assert.Nil(t, err)
}

func TestStartAgentWithCustomEcho(t *testing.T) {
	agent, err := NewAgentEchoHandler(
		WithCustomEcho(nil),
	)
	assert.Nil(t, err)
	assert.NotNil(t, agent)

	assert.Nil(t, err)
}

func TestStartAgentWithCustomConfig(t *testing.T) {
	cfg := config.NewConfig()

	agent, err := NewAgentEchoHandler(
		WithCustomConfig(cfg),
	)
	assert.Nil(t, err)
	assert.NotNil(t, agent)

	assert.Nil(t, err)
}