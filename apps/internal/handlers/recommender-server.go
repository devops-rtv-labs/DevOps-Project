// Recommender Server
// This is the implementation of the Recommender Handler
package handlers

import (
	"encoding/json"

	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common/repositories"
	"go.uber.org/zap"
)

// RecommenderHandler is a struct that implements the RecommenderHandler interface
type RecommenderHandler struct {
	mq repositories.MessageQueueRepository
	service core.RecommenderService
	logger *zap.SugaredLogger
}

// RecommenderConfiguration is a function that configures a RecommenderHandler
type RecommenderConfiguration func (r *RecommenderHandler) error

// NewRecommenderHandler creates a new RecommenderHandler
// It accepts a list of RecommenderConfiguration functions that will configure the handler
// It returns a RecommenderHandler and an error
func NewRecommenderHandler(cfgs ...RecommenderConfiguration) (*RecommenderHandler, error) {
	r := &RecommenderHandler{}
	for _, cfg := range cfgs {
		err := cfg(r)
		if err != nil {
			return nil, err
		}
	}

	if r.logger == nil {
		r.logger = common.GetLogger()
	}
	
	return r, nil
}

// Start starts the RecommenderHandler
// It subscribes to the feelings queue and publishes to the recommendations queue
// It returns an error
func (r *RecommenderHandler) Start() error {
	r.logger.Infof("[*] starting recommender handler")
	feelings, err := r.mq.Subscribe(common.FEELINGS_QUEE_NAME)
	if err != nil {
		return err
	}

	for b := range feelings {
		feeling := core.Feeling{}
		err := json.Unmarshal(b, &feeling)

		if err != nil {
			return err
		}
		
		recommendation, err := r.service.Recommend(feeling)
		if err != nil {
			return err
		}

		err = r.mq.Publish(common.RECOMMENDATIONS_QUEE_NAME, []byte(recommendation.String()))

		if err != nil {
			return err
		}
	}

	return nil
}

// WithMessageQueueRepository configures the Recommender to use a MessageQueueRepository
func WithMessageQueueRepository(mq repositories.MessageQueueRepository) RecommenderConfiguration {
	return func(a *RecommenderHandler) error {
		a.mq = mq
		return nil
	}
}

// WithRecommenderService configures the Recommender to use a RecommenderService
func WithRecommenderService(service core.RecommenderService) RecommenderConfiguration {
	return func(a *RecommenderHandler) error {
		a.service = service
		return nil
	}
}