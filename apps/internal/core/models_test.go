package core

import (
	"testing"

	"github.com/stretchr/testify/assert"
)


func TestFeelingToString(t  *testing.T) {
	feeling := Feeling{Feeling: "wanna feel like utopia"}

	assert.Equal(t, feeling.String(), "{\"feeling\":\"wanna feel like utopia\"}")
}

func TestRecommendationToString(t  *testing.T) {
	recommendation := Recommendation{Places: []Place{{Name: "Circus Maximus"}}}

	assert.Equal(t, recommendation.String(), "{\"places\":[{\"name\":\"Circus Maximus\"}]}")
}

func TestPlaceToString(t  *testing.T) {
	place := Place{Name: "Circus Maximus"}

	assert.Equal(t, place.String(), "{\"name\":\"Circus Maximus\"}")
}