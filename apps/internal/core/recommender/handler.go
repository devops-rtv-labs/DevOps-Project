// RecommenderHandler Interface
// This file contains the interface that defines the recommender handler
package recommender

// RecommenderHandler is an interface that defines the recommender handler
type RecommenderHandler interface {

	// Start starts the RecommenderHandler
	Start() error
}