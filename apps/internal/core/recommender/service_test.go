package recommender

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core"
)

func TestCreateRecommenderService(t *testing.T) {
	assert.NotPanics(t, func () {
		service, err := NewRecommenderServiceImpl()
		assert.Nil(t, err)
		assert.NotNil(t, service)
	})
}

func TestFailedREcommenderService(t *testing.T) {
	assert.NotPanics(t, func () {
		service, err := NewRecommenderServiceImpl(
			func (a *RecommenderServiceImpl) error {
				return errors.New("error")
			},
		)
		assert.NotNil(t, err)
		assert.Nil(t, service)
	})
}

func TestRecommend(t *testing.T) {
	service, err := NewRecommenderServiceImpl()
	assert.Nil(t, err)
	assert.NotNil(t, service)

	recommendation, err := service.Recommend(core.Feeling{Feeling: "wanna feel like utopia"})
	assert.Nil(t, err)
	assert.NotNil(t, recommendation)
	assert.Equal(t, recommendation.String(), "{\"places\":[{\"name\":\"Circus Maximus\"}]}")
}