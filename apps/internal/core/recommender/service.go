// RecommenderServiceImpl
// It implements the RecommenderService interface
package recommender

import (
	"strings"
	"time"

	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"go.uber.org/zap"
)

// RecommenderServiceImpl is a struct that implements the RecommenderService interface
type RecommenderServiceImpl struct {
	logger *zap.SugaredLogger
}

// RecommenderServiceImplConfiguration is a function that configures a RecommenderServiceImpl
type RecommenderServiceImplConfiguration func (a *RecommenderServiceImpl) error

// NewRecommenderServiceImpl creates a new RecommenderServiceImpl
// It accepts a list of RecommenderServiceImplConfiguration functions that will configure the service
// It returns a RecommenderServiceImpl and an error
func NewRecommenderServiceImpl(cfgs ...RecommenderServiceImplConfiguration) (*RecommenderServiceImpl, error) {
	s := &RecommenderServiceImpl{}

	for _, cfg := range cfgs {
		err := cfg(s)
		if err != nil {
			return nil, err
		}
	}

	if s.logger == nil {
		s.logger = common.GetLogger()
	}

	return s, nil
}

// Recommend recommends places for a feeling
// It accepts a feeling
// It returns a recommendation and an error
func (h *RecommenderServiceImpl) Recommend(feeling core.Feeling) (core.Recommendation, error) {
	h.logger.Infof("[*] recommending places for feeling: %s", feeling.String())
	time.Sleep(1 * time.Second)

	words := h._analyseFeeling(feeling)

	places := h._selectPlaces(words)

	r := core.Recommendation{}
	r.Places = places

	return r, nil
}

// _analyseFeeling analyses a feeling
// It accepts a feeling
// It returns a list of words
func (h *RecommenderServiceImpl) _analyseFeeling(feeling core.Feeling) []string {
	h.logger.Infof("[*] analysing feeling: %s", feeling.String())
	words := strings.Split(feeling.Feeling, " ")
	distinctWords := make(map[string]bool)

	for _, word := range words {
		distinctWords[word] = true
	}

	var distinctWordsSlice []string
	for word := range distinctWords {
		distinctWordsSlice = append(distinctWordsSlice, word)
	}

	return distinctWordsSlice
}

// _selectPlaces selects places for a list of words
// It accepts a list of words
// It returns a list of places
func (h *RecommenderServiceImpl) _selectPlaces(words []string) []core.Place {
	h.logger.Infof("[*] selecting places for words: %s", strings.Join(words, ", "))
	places := []core.Keyword{
		{ Place: "Baristas Aouina", Keyword: "coffee" },
		{ Place: "ParadIce", Keyword: "ice cream" },
		{ Place: "La Closerie", Keyword: "restaurant" },
		{ Place: "La Closerie", Keyword: "bar" },
		{ Place: "La Closerie", Keyword: "cafe" },
		{ Place: "La Sigale", Keyword: "hotel" },
		{ Place: "Circus Maximus", Keyword: "utopia" },
	}

	var selectedPlaces []core.Place
	for _, place := range places {
		for _, word := range words {
			if place.Keyword == word {
				selectedPlaces = append(selectedPlaces, core.Place{
					Name: place.Place,
				})
			}
		}
	}

	return selectedPlaces
}