package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewConfig(t *testing.T) {
	t.Run("should return a new config", func(t *testing.T) {
		config := NewConfig()

		assert.NotNil(t, config)
	})
}

func TestGetApiServerPort(t *testing.T) {
	t.Run("should return the api server port", func(t *testing.T) {
		config := NewConfig()

		assert.Equal(t, "8080", config.GetApiServerPort())
	})
}

func TestGetRabbitMQ(t *testing.T) {
	t.Run("should return the rabbitmq config", func(t *testing.T) {
		config := NewConfig()

		assert.NotNil(t, config.GetRabbitMQ())
	})
}