package config

import (
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
)

type RabbitMQConfig struct {
	Host string
	Port string
	Username string
	Password string
}

func NewRabbitMQConfig() *RabbitMQConfig {
	return &RabbitMQConfig{
		Host: common.GetEnv("RABBITMQ_HOST", "localhost"),
		Port: common.GetEnv("RABBITMQ_PORT", "5672"),
		Username: common.GetEnv("RABBITMQ_USERNAME", "guest"),
		Password: common.GetEnv("RABBITMQ_PASSWORD", "guest"),
	}
}

func (c *RabbitMQConfig) GetHost() string {
	return c.Host
}

func (c *RabbitMQConfig) GetPort() string {
	return c.Port
}

func (c *RabbitMQConfig) GetUsername() string {
	return c.Username
}

func (c *RabbitMQConfig) GetPassword() string {
	return c.Password
}