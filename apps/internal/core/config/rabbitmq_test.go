package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRabbitMQConfig(t *testing.T) {
	t.Run("should return a new rabbitmq config", func(t *testing.T) {
		config := NewRabbitMQConfig()

		assert.NotNil(t, config)
	})
}

func TestGetRabbitMQHost(t *testing.T) {
	t.Run("should return the host", func(t *testing.T) {
		config := NewRabbitMQConfig()

		assert.Equal(t, "localhost", config.GetHost())
	})
}

func TestGetRabbitMQPort(t *testing.T) {
	t.Run("should return the port", func(t *testing.T) {
		config := NewRabbitMQConfig()

		assert.Equal(t, "5672", config.GetPort())
	})
}

func TestGetRabbitMQUsername(t *testing.T) {
	t.Run("should return the username", func(t *testing.T) {
		config := NewRabbitMQConfig()

		assert.Equal(t, "guest", config.GetUsername())
	})
}

func TestGetRabbitMQPassword(t *testing.T) {
	t.Run("should return the password", func(t *testing.T) {
		config := NewRabbitMQConfig()

		assert.Equal(t, "guest", config.GetPassword())
	})
}
