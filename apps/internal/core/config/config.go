package config

import (
	"github.com/joho/godotenv"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
)

type Config struct {
	ApiServerPort string
	RabbitMQ RabbitMQConfig
}

func NewConfig() *Config {
	return &Config{
		ApiServerPort: common.GetEnv("API_SERVER_PORT", "8080"),
		RabbitMQ: *NewRabbitMQConfig(),
	}
}

func (c *Config) GetApiServerPort() string {
	return c.ApiServerPort
}

func (c *Config) GetRabbitMQ() RabbitMQConfig {
	return c.RabbitMQ
}

func init() {
	godotenv.Load()
}