package agent

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core"
	"gitlab.com/devops-rtv-labs/DevOps-Project/pkg/repositories"
)

func TestCreateAgentService(t *testing.T) {
	assert.NotPanics(t, func() {
		service, err := NewAgentServiceImpl()
		assert.Nil(t, err)
		assert.NotNil(t, service)
	})
}

func TestFailedAgentService(t *testing.T) {
	assert.NotPanics(t, func() {
		service, err := NewAgentServiceImpl(
			func(a *AgentServiceImpl) error {
				return errors.New("error")
			},
		)
		assert.NotNil(t, err)
		assert.Nil(t, service)
	})
}

func TestSubmitWantedFeeling(t *testing.T) {
	mq, err := repositories.NewFakeMessageQueueRepository()

	assert.Nil(t, err)
	assert.NotNil(t, mq)

	service, err := NewAgentServiceImpl(
		WithMessageQueueRepository(mq),
	)
	assert.Nil(t, err)
	assert.NotNil(t, service)

	feeling := core.Feeling{Feeling: "wanna feel like utopia"}
	err = service.SubmitWantedFeeling(feeling)
	assert.Nil(t, err)
}

func TestStreamRecommendations(t *testing.T) {
	mq, err := repositories.NewFakeMessageQueueRepository()

	assert.Nil(t, err)
	assert.NotNil(t, mq)

	service, err := NewAgentServiceImpl(
		WithMessageQueueRepository(mq),
	)
	assert.Nil(t, err)
	assert.NotNil(t, service)

	feeling := core.Feeling{Feeling: "wanna feel like utopia"}
	err = service.SubmitWantedFeeling(feeling)
	assert.Nil(t, err)

	channel, err := service.StreamRecommendations()
	assert.Nil(t, err)
	assert.NotNil(t, channel)

	recommendation := <-channel
	assert.NotNil(t, recommendation)
}