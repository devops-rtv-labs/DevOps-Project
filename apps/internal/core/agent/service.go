// Agent Service Implementation
// This file contains the implementation of the agent service
package agent

import (
	"encoding/json"

	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common"
	"gitlab.com/devops-rtv-labs/DevOps-Project/internal/core/common/repositories"
	"go.uber.org/zap"
)

// AgentServiceImpl is a struct that implements the AgentService interface
type AgentServiceImpl struct {
	mq repositories.MessageQueueRepository
	logger *zap.SugaredLogger
}

// AgentServiceImplConfiguration is a function that configures a AgentServiceImpl
type AgentServiceImplConfiguration func (a *AgentServiceImpl) error

// NewAgentServiceImpl creates a new AgentServiceImpl
// It accepts a list of AgentServiceImplConfiguration functions that will configure the service
// It returns a AgentServiceImpl and an error
func NewAgentServiceImpl(cfgs ...AgentServiceImplConfiguration) (*AgentServiceImpl, error) {
	s := &AgentServiceImpl{}

	for _, cfg := range cfgs {
		err := cfg(s)
		if err != nil {
			return nil, err
		}
	}

	if s.logger == nil {
		s.logger = common.GetLogger()
	}

	s.logger.Infof("[*] agent service initialized")

	return s, nil
}


// SubmitWantedFeeling submits a wanted feeling
// It accepts a feeling
// It returns an error
func (h *AgentServiceImpl) SubmitWantedFeeling(feeling core.Feeling) error {
	h.logger.Infof("[*] submitting wanted feeling: %s", feeling.String())

	err := h.mq.Publish(common.FEELINGS_QUEE_NAME, []byte(feeling.String()))

	if err != nil {
		return err
	}

	return nil
}

// StreamRecommendations streams recommendations
// It returns a channel of recommendations and an error
func (h *AgentServiceImpl) StreamRecommendations() (<-chan core.Recommendation, error) {
	h.logger.Infof("[*] streaming recommendations")

	forwarded := make(chan core.Recommendation)
	incoming, err := h.mq.Subscribe(common.RECOMMENDATIONS_QUEE_NAME)

	if err != nil {
		return nil, err
	}

	go func() {
		for b := range incoming {
			recommendation := core.Recommendation{}
			err := json.Unmarshal(b, &recommendation)

			if err != nil {
				h.logger.Errorf("[!] failed to unmarshal recommendation: %s", err.Error())
				continue
			}

			forwarded <- recommendation
		}
	}()


	return forwarded, nil
}

// WithMessageQueueRepository configures the AgentService to use a MessageQueueRepository
func WithMessageQueueRepository(mq repositories.MessageQueueRepository) AgentServiceImplConfiguration {
	return func(a *AgentServiceImpl) error {
		a.mq = mq
		return nil
	}
}