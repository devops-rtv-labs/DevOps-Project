// Agent Handler Interface
// This file contains the interface that defines the agent handler
package agent

// AgentHandler is an interface that defines the agent handler
type AgentHandler interface {

	// SubmitWantedFeeling submits a wanted feeling
	SubmitWantedFeeling()

	// StreamRecommendations streams recommendations
	StreamRecommendations()

	// Start starts the AgentHandler
	// It returns an error
	Start() error
}