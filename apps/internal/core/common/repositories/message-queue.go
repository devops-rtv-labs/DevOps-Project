// Message Queue Repository Interface
// This file contains the interface that defines the message queue repository
package repositories

// MessageQueueRepository is an interface that defines the message queue repository
type MessageQueueRepository interface {

	// Publish publishes a message to a queue
	// It accepts a queue name and a message
	// It returns an error
	Publish(queue string, message []byte) error

	// Subscribe subscribes to a queue
	// It accepts a queue name
	// It returns a channel of messages and an error
	Subscribe(queue string) (<-chan []byte, error)
}