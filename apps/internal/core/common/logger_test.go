package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)


func TestNewLogger(t  *testing.T) {
	logger, clean := NewLogger()
	defer clean()

	assert.NotNil(t, logger)
}

func TestGetLogger(t  *testing.T) {
	logger := GetLogger()

	assert.NotNil(t, logger)
}

func TestGetLoggerWithNilLogger(t  *testing.T) {
	logger = nil
	logger := GetLogger()

	assert.NotNil(t, logger)
}