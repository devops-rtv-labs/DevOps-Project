package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)


func TestGetEnv(t  *testing.T) {
	t.Run("should return the value of the environment variable", func(t *testing.T) {
		value := GetEnv("PATH", "default")

		assert.NotNil(t, value)
	})

	t.Run("should return the default value", func(t *testing.T) {
		value := GetEnv("NOT_EXISTING", "default")

		assert.Equal(t, "default", value)
	})
}
