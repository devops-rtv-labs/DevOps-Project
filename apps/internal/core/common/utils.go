// Common Constants
// This file contains the common constants and utilities used by the application
package common

import (
	"os"
)

const (
	// FEELINGS_QUEE_NAME is the name of the feelings queue
	FEELINGS_QUEE_NAME = "wanted-feelings"

	// RECOMMENDATIONS_QUEE_NAME is the name of the recommendations queue
	RECOMMENDATIONS_QUEE_NAME = "recommendations"
)


// GetEnv returns the value of the environment variable with the given name
// If the environment variable is not set, it returns the given default value
func GetEnv(name, defaultValue string) string {
	value := os.Getenv(name)
	if value == "" {
		return defaultValue
	}

	return value
}

