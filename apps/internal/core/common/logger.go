// Logger
// The logger is a simple wrapper around zap, a fast, structured, leveled logging library for Go.
package common

import "go.uber.org/zap"

// logger is the logger instance, using zap
var logger *zap.SugaredLogger


// NewLogger creates a new logger
func NewLogger() (*zap.SugaredLogger, func()) {
	_logger, _ := zap.NewProduction()
	sugar := _logger.Sugar()
	logger = sugar

	logger.Info("[*] logger initialized")
	
	return sugar, func() {
		_logger.Sync()
	}
}

// GetLogger returns the logger instance
func GetLogger() *zap.SugaredLogger {
	if logger == nil {
		logger, _ = NewLogger()
	}

	return logger
}