// Core Application Ports
// This file contains the interfaces that define the ports of the core application
package core

// AgentService is an interface that defines the agent service
type AgentService interface {
	
	// SubmitWantedFeeling submits a wanted feeling
	// It accepts a feeling
	// It returns an error
	SubmitWantedFeeling(feeling Feeling) error

	// SubmitFeeling submits a feeling
	// It accepts a feeling
	// It returns a channel of recommendations and an error
	StreamRecommendations() (<-chan Recommendation, error)
}


// RecommenderService is an interface that defines the recommender service
type RecommenderService interface {

	// Recommend recommends places for a feeling
	// It accepts a feeling
	// It returns a recommendation and an error
	Recommend(feeling Feeling) (Recommendation, error)
}