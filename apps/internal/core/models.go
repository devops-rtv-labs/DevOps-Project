// Domain models
// This file contains the domain models of the core application
// The models are: Feeling, Recommendation, Place, Keyword
package core

import "encoding/json"

// Feeling is a struct that represents a feeling
type Feeling struct {
	Feeling string `json:"feeling" example:"wanna feel like utopia"`
}

// String returns the string representation of a Feeling
func (f *Feeling) String() string {
	b, _ := json.Marshal(f)
	return string(b)
}

// Recommendation is a struct that represents a recommendation
type Recommendation struct {
	Places []Place `json:"places"`
}

// String returns the string representation of a Recommendation
func (r *Recommendation) String() string {
	b, _ := json.Marshal(r)
	return string(b)
}

// Place is a struct that represents a place
type Place struct {
	Name string `json:"name" example:"Circus Maximus"`
}

// String returns the string representation of a Place
func (p *Place) String() string {
	b, _ := json.Marshal(p)
	return string(b)
}

// Keyword is a struct that represents a place assigned to a keyword
type Keyword struct {
	Keyword string `json:"keyword"`
	Place   string  `json:"place"`
}