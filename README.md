# Final DevOps Project

## Introduction

This project is a deliverable for the DevOps course at the National Institute of Applied Science and Technology (INSAT). It consists of a web application, a CI/CD pipeline, Infrastructure as Code and a monitoring system.

## Contributing

To contribute to this project, please follow [this guide](CONTRIBUTING.md).

## How to use

Run `make validate` to validate the prerequisites. If you have any missing prerequisites, please follow the instructions in the output of the command.

Run `make init` to initialize the development environment. This will create: 
- a Kind cluster
- an HAProxy Ingress Controller

Run `make test` to run the tests. and generate the coverage report. Tests include:
- Unit tests
- Integration tests

### Build

Run `make build` to build all the artifacts, Which include:
- Agent API Server (`api-server`)
- Recommender Server (`recommender`)
- Frontend (`web`)
- Storybook (`storybook`)
- Swagger Documentation (`swagger`)

You can also build each artifact individually by running `make build-<artifact>`.

### `docker-compose up`

When you run `docker-compose up`, the following containers will be created:
- `rabbitmq` Instance, where a web interface will be available at `http://localhost:15672`
- `api-server` (Agent API Server)
- `recommender` (Recommender Server)
- `web` (Frontend), which will be available at `http://localhost:8082`
- `storybook` (Storybook), which will be available at `http://localhost:8081`
- `swagger` (Swagger Documentation), which will be available at `http://localhost:8083`
- `godoc` (Go Documentation), which will be available at `http://localhost:6060/pkg/devops-rtv-labs/?m=all`