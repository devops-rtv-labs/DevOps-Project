// HAProxy Ingress Controller
resource "helm_release" "haproxy-ingress-controller" {
  depends_on = [kind_cluster.development-cluster]
  
  name       = "haproxy-kubernetes-ingress"

  repository = "https://haproxytech.github.io/helm-charts"
  chart      = "kubernetes-ingress"

  namespace  = "haproxy-controller"
  create_namespace = true

  set {
    name  = "controller.kind"
    value = "DaemonSet"
  }

    set {
        name  = "controller.daemonset.useHostPort"
        value = "true"
    }

    set {
        name  = "controller.service.nodePorts.http"
        value = "31000"
    }
}