// Kind Cluster for Development
resource "kind_cluster" "development-cluster" {
    name = "devops-rt5"
    wait_for_ready = true

    kind_config {
      kind        = "Cluster"
      api_version = "kind.x-k8s.io/v1alpha4"

      node {
          role = "control-plane"

          // Ingress Controller
          extra_port_mappings {
              container_port = 31000
              host_port      = 31000
          }

          // Vault UI (Since It does not support subpath)
          extra_port_mappings {
              container_port = 31001
              host_port      = 8200
          }


          // SonarQube
          extra_port_mappings {
              container_port = 31002
              host_port      = 9000
          }

          // ArgoCD
          extra_port_mappings {
              container_port = 31003
              host_port      = 9001
          }
      }

      node {
          role = "worker"
      }

      node {
          role = "worker"
      }
  }

}