terraform {
  required_providers {
    kind = {
       source = "tehcyx/kind"
        version = "0.2.1"
    }

    command = {
      source = "hkak03key/command"
    }
  }
}

# Providers Configuration
provider "kind" {}

provider "command" {}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}
