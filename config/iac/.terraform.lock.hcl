# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/helm" {
  version = "2.12.1"
  hashes = [
    "h1:sgYI7lwGqJqPopY3NGmhb1eQ0YbH8PIXaAZAmnJrAvw=",
    "zh:1d623fb1662703f2feb7860e3c795d849c77640eecbc5a776784d08807b15004",
    "zh:253a5bc62ba2c4314875139e3fbd2feaad5ef6b0fb420302a474ab49e8e51a38",
    "zh:282358f4ad4f20d0ccaab670b8645228bfad1c03ac0d0df5889f0aea8aeac01a",
    "zh:4fd06af3091a382b3f0d8f0a60880f59640d2b6d9d6a31f9a873c6f1bde1ec50",
    "zh:6816976b1830f5629ae279569175e88b497abbbac30ee809948a1f923c67a80d",
    "zh:7d82c4150cdbf48cfeec867be94c7b9bd7682474d4df0ebb7e24e148f964844f",
    "zh:83f062049eea2513118a4c6054fb06c8600bac96196f25aed2cc21898ec86e93",
    "zh:a79eec0cf4c08fca79e44033ec6e470f25ff23c3e2c7f9bc707ed7771c1072c0",
    "zh:b2b2d904b2821a6e579910320605bc478bbef063579a23fbfdd6fcb5871b81f8",
    "zh:e91177ca06a15487fc570cb81ecef6359aa399459ea2aa7c4f7367ba86f6fcad",
    "zh:e976bcb82996fc4968f8382bbcb6673efb1f586bf92074058a232028d97825b1",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hkak03key/command" {
  version = "0.1.1"
  hashes = [
    "h1:PtyZEa6fw8OwF3MicoMPK3xmz9FYvTLL2Y4tmzYwTgc=",
    "zh:067f9cc78e7e3d1a27a7f138a5aaf5b0d6246b164be4e35c0f9617d86a690f9e",
    "zh:1e3d49ad0afd223fe3fb7db3a3cd0eeb2c0a92d2abb5043c347baaa4e76ddec2",
    "zh:27fdd0efa8b4a4a73ee9206b23ed0cc878dafb5c741a819310a8e937337f838c",
    "zh:4999e34fbbd74c4942b24977c494ee6d1193f3090fda2a4ca0e5d36bbd9783b9",
    "zh:49a1d31338b042053f8133e770934c375f4f4ca803fc2c1a7b200fbfa27d95d6",
    "zh:86eaa05d2cce66e6813bf7cfc4289a0d909f1019b7af58999a5be88386c67d50",
    "zh:9698b3fc4051a88a3085ba875552e041eb4c6e17a245e5346901f53dc7fddeb2",
    "zh:a5d4127d51a961a8506454b11b013e3fd2572f151e03b55b73528b101dacca70",
    "zh:ab2ca3f7883bdbc4769b56aec50ed138456b5f37bfab619e11d42eccf27aeaf5",
    "zh:e23bf61abbb2ec945b72c06211e92e98f42249e33b3f63a5d749840c0640ce3a",
    "zh:e3e8c2737d530a17296a629fe875b4b89c52d2a4011ffce266d011ea112a6efe",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fb6a6449a9941c4d8a6fb5a0d5ea69e288c52031a439b2b0f8aa0952d52da036",
  ]
}

provider "registry.terraform.io/tehcyx/kind" {
  version     = "0.2.1"
  constraints = "0.2.1"
  hashes = [
    "h1:t6agiLTeQmVK+uQrHt9cbfpjvvtdlq+cnudhlRoTjjM=",
    "zh:24eb1b94c4669534d6bca1a5d2b0deb9c58989cb31cee9104d2074525faead59",
    "zh:98541edd176cad90deb2d4d3c596f43e899a9b387afc0957a8c3b9007dcbc335",
    "zh:a963e39769f126ba9a75b6f186ccc235712bd015dd4071d2b0c2e0c135576850",
    "zh:c83cce2e837bc386f589d10828cb1c375bc9a6aa377eb15523f3a7852d33480f",
    "zh:f6477f80e4715a4706403a8a02beefefc9f84f59d27285d81ce5595608ba340d",
    "zh:ff9f2e35f7efef7cc01b014223049765f4cc5e8d2e55c97a0c8ad310b7fe1c8d",
  ]
}
