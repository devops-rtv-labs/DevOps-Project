FROM golang:alpine3.18

WORKDIR /main

RUN apk add g++ && apk add make

COPY Makefile .

COPY apps/go.mod apps/go.sum ./apps/

WORKDIR /main/apps

RUN go mod download

COPY apps .

WORKDIR /main

RUN make build-swagger

RUN chmod +x ./bin/swagger

EXPOSE 8081

CMD ["./bin/swagger"]