FROM node:18-alpine as build

WORKDIR /main

RUN apk add g++ && apk add make

COPY Makefile .

COPY apps/web/package*.json ./apps/web/

WORKDIR /main/apps/web

RUN npm install

COPY apps/web .

WORKDIR /main

ENV VITE_WS_URL=ws://api-server:8080/ws 

RUN make build-web-app

FROM nginx:1.16.0-alpine

ARG API_HOST

ARG API_PORT

ENV API_HOST=$API_HOST

ENV API_PORT=$API_PORT

RUN rm -rf /usr/share/nginx/html/*

COPY --from=build /main/dist/web /usr/share/nginx/html

COPY apps/web/nginx.conf /etc/nginx/conf.d

RUN mv /etc/nginx/conf.d/nginx.conf /etc/nginx/conf.d/nginx.conf.tmp

COPY apps/web/docker-entrypoint.sh /

RUN chmod +x /docker-entrypoint.sh

EXPOSE 80

ENTRYPOINT ["/docker-entrypoint.sh"]