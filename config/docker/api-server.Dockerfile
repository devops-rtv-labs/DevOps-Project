FROM golang:alpine3.18

WORKDIR /main

RUN apk add g++ && apk add make

COPY Makefile .

COPY apps/go.mod apps/go.sum ./apps/

WORKDIR /main/apps

RUN go mod download

COPY apps .

WORKDIR /main

RUN make build-api-server

RUN chmod +x ./bin/api-server

EXPOSE 8080

CMD ["./bin/api-server"]