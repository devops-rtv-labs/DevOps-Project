FROM node:18-alpine as build

WORKDIR /main

RUN apk add g++ && apk add make

COPY Makefile .

COPY apps/web/package*.json ./apps/web/

WORKDIR /main/apps/web

RUN npm install

COPY apps/web .

WORKDIR /main

RUN make build-storybook

FROM nginx:1.16.0-alpine

RUN rm -rf /usr/share/nginx/html/*

COPY --from=build /main/dist/storybook /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]