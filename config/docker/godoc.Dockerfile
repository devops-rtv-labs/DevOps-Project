FROM golang:alpine3.18

RUN mkdir -p /go/src/devops-rtv-labs

WORKDIR /usr/local/go/src/devops-rtv-labs

RUN go install golang.org/x/tools/cmd/godoc@latest

COPY ./apps/ .

WORKDIR /usr/local/go/src/

CMD ["godoc", "-http=:6060"]